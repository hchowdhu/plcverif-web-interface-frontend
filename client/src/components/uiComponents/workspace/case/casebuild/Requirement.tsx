import flexClasses from "../../../../FlexFlowControl.module.css"
import { useContext, useEffect, useRef, useState } from "react";
import CaseBuildingContext from "../../../../../store/caseBuilding-context";
import ColorThemeContext from "../../../../../store/color-theme-context";

function Requirement() {

    const caseBuildingCtx = useContext(CaseBuildingContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const reqReqtypeRef = useRef(null);
    const reqReqtypeAssChecksRef = useRef(null);
    const reqReqtypePatPatRef = useRef(null);
    const reqReqtypePat1Ref = useRef(null);
    const reqReqtypePat2Ref = useRef(null);
    const reqReqtypePat3Ref = useRef(null);
    const inputVariableRef = useRef(null);

    const [selectedRequirement, setSelectedRequirement] = useState(caseBuildingCtx.requirement.type == "" ? "Assertion" : caseBuildingCtx.requirement.type);
    const [numberOfPatternReqTextAreas, setNumberOfTextAreas] = useState(computePatternNumberOfTextAreas(caseBuildingCtx.requirement.pattern == "" ? "pattern-implication" : caseBuildingCtx.requirement.pattern));
    const [assertions, setAssertions] = useState(caseBuildingCtx.requirement.checks as string[]);
    const [inputVars, setInputVars] = useState(caseBuildingCtx.requirement.inputVariables as string[]);

    function updateSelectedRequirement(event: any) {
        setSelectedRequirement(event.target.value);
    }

    function computePatternNumberOfTextAreas(value: string) {
        switch(value) {
            case "pattern-implication":
                return 2;
                break;
            case "pattern-invariant":
                return 1;
                break;
            case "pattern-forbidden":
                return 1;
                break;
            case "pattern-statechange-duringcycle":
                return 2;
                break;
            case "pattern-statechange-betweencycles":
                return 3;
                break;
            case "pattern-reachability":
                return 1;
                break;
            case "pattern-repeatability":
                return 1;
                break;
            case "pattern-leadsto":
                return 2;
                break;
            default:
                return 1;
        }
    }

    function updatePatternNumberOfTextAreas(event: any) {
        setNumberOfTextAreas(computePatternNumberOfTextAreas(event.target.value));
    }

    function saveMetaDataInfo() {
        const reqReqtypeRefVAL = reqReqtypeRef.current == null ? "" : reqReqtypeRef.current["value"];
        const reqReqtypePatPatRefVAL = reqReqtypePatPatRef.current == null ? "" : reqReqtypePatPatRef.current["value"];
        const reqReqtypePat1RefVAL = reqReqtypePat1Ref.current == null ? "" : reqReqtypePat1Ref.current["value"];
        const reqReqtypePat2RefVAL = reqReqtypePat2Ref.current == null ? "" : reqReqtypePat2Ref.current["value"];
        const reqReqtypePat3RefVAL = reqReqtypePat3Ref.current == null ? "" : reqReqtypePat3Ref.current["value"];
        
        if(reqReqtypeRefVAL.toString() == "Assertion"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
                "checks": assertions,
                "inputVariables": inputVars
            });
        } else if(reqReqtypeRefVAL.toString() == "Division by zero"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
                "inputVariables": inputVars
            });
        } else if(reqReqtypeRefVAL.toString() == "Pattern"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
                "pattern": reqReqtypePatPatRefVAL,
                "one": reqReqtypePat1RefVAL,
                "two": reqReqtypePat2RefVAL,
                "three": reqReqtypePat3RefVAL,
                "inputVariables": inputVars
            });
        }
    }

    function addInputVariable() {
        const inputVariableRefVAL = inputVariableRef.current == null ? "" : inputVariableRef.current["value"];
        var modifiedInputVars: string[] = caseBuildingCtx.requirement.inputVariables;

        if(!modifiedInputVars.includes(inputVariableRefVAL) && inputVariableRef.current) { // only add if it does not exist
            modifiedInputVars.push(inputVariableRefVAL);
        }

        setInputVars(modifiedInputVars);
        saveMetaDataInfo();
    }

    function clearInputVariables() {
        setInputVars([]);
        saveMetaDataInfo();
    }

    function addAssertion() {
        const reqReqtypeAssChecksRefVAL = reqReqtypeAssChecksRef.current == null ? "" : reqReqtypeAssChecksRef.current["value"];
        var modifiedAssertions: string[] = caseBuildingCtx.requirement.checks;

        if(!modifiedAssertions.includes(reqReqtypeAssChecksRefVAL) && reqReqtypeAssChecksRef.current) { // only add if it does not exist
            modifiedAssertions.push(reqReqtypeAssChecksRefVAL);
        }

        setAssertions(modifiedAssertions);
        saveMetaDataInfo();
    }

    function clearAssertions() {
        setAssertions([]);
        saveMetaDataInfo();
    }

    // Without these useEffects, the clearing button won't update the data in caseBuildingCtx
    useEffect(function() {
        saveMetaDataInfo();
    }, [inputVars]);

    useEffect(function() {
        saveMetaDataInfo();
    }, [assertions]);

    return (
        <div className="card text-center" style = {{height: "100%", width: "85%", left: "7.5%", background: colorThemeCtx.caseBuildContentBackground, color: colorThemeCtx.darkModeTextColor}}>
            <div className="card-header" style = {{background: colorThemeCtx.caseBuildHeaderBackground}}>
                <h5 style = {{marginTop: "1%"}}>4. Requirement</h5>
            </div>
            <div className = {["card-body", flexClasses.caseBuildContentFlexControl].join(" ")}>
                <div className = "card" style = {{width: "100%"}} onChange={saveMetaDataInfo}>
                    <div className = "card-header">Choose your requirement type here</div>
                    <div className = "card-body">
                        <label className="form-label">Requirement Type:</label>
                        <select defaultValue = {caseBuildingCtx.requirement.type} className="form-select" aria-label="Default select example" onChange = {updateSelectedRequirement} ref = {reqReqtypeRef}>
                            <option value="Assertion" selected>Assertion</option>
                            <option value="Division by zero">Division by zero</option>
                            <option value="Pattern">Pattern</option>
                        </select>

                        <br></br>

                        {
                            selectedRequirement == "Assertion" ?
                                <div className="row">
                                    <div className="col-2">
                                        <button type="button" className="btn btn-info" onClick = {clearAssertions}>Clear</button>
                                    </div>
                                    <div className="col-4" style = {{height: "100%"}}>
                                        {
                                            (assertions.length > 0) ?
                                                <ul className="list-group" style = {{maxHeight: "10vh", overflow: "auto"}}>
                                                    {
                                                        assertions.map(assertion => (
                                                            <li key = {assertion} className="list-group-item">{assertion}</li>
                                                        ))
                                                    }
                                                </ul>
                                            :
                                            "No assertions created yet"
                                        }
                                    </div>
                                    <div className="col-4">
                                        <input ref = {reqReqtypeAssChecksRef} type="text" className="form-control" id="exampleFormControlInput2" autoComplete="off" placeholder="Enter an assertion" />
                                    </div>
                                    <div className="col-2">
                                        <button type="button" className="btn btn-info" onClick = {addAssertion}>Add</button>
                                    </div>
                                </div>
                                : selectedRequirement == "Division by zero" ?
                                    <div>
                                        <label className="form-label"><small>No further input required!</small></label>
                                    </div>
                                    : selectedRequirement == "Pattern" ?
                                        <div>
                                            <label className="form-label">Pattern:</label>
                                            <select onChange = {updatePatternNumberOfTextAreas} defaultValue = {caseBuildingCtx.requirement.pattern} className="form-select" aria-label="Default select example" ref = {reqReqtypePatPatRef} style = {{fontSize: "0.7vw"}} >
                                                <option value="pattern-implication" selected>If (1) is true at the end of the PLC cycle, then (2) should always be true at the end of the same cycle.</option>
                                                <option value="pattern-invariant">(1) is always true at the end of the PLC cycle.</option>
                                                <option value="pattern-forbidden">(1) is impossible at the end of the PLC cycle.)</option>
                                                <option value="pattern-statechange-duringcycle">If (1) is true at the beginning of the PLC cycle, then (2) is always true at the end of the same cycle.</option>
                                                <option value="pattern-statechange-betweencycles">If (1) is true at the end of the cycle N and (2) is true at the end of the cycle N+1, then (3) is always true at the end of cycle N+1</option>
                                                <option value="pattern-reachability">It is possible to have (1) at the end of the cycle.</option>
                                                <option value="pattern-repeatability">Any time it is possible to have eventually (1) at the end of a cycle.</option>
                                                <option value="pattern-leadsto">If (1) is true at the end of a cycle, (2) was true at the end of an earlier cycle.</option>
                                            </select>

                                            <div className="container text-center">
                                                <div className="row">
                                                    <div className="col" style = {{padding: "1px"}}>
                                                        <label htmlFor="exampleFormControlTextarea9" className="form-label" style = {{paddingTop: "1rem"}}>1:</label>
                                                        <textarea defaultValue = {caseBuildingCtx.requirement.one} className="form-control" id="exampleFormControlTextarea9" rows={5} style = {{whiteSpace: "nowrap", resize: "none", width: "100%"}} ref = {reqReqtypePat1Ref}></textarea>
                                                    </div>
                                                    {numberOfPatternReqTextAreas >= 2 ? 
                                                        <div className="col" style = {{padding: "1px"}}>
                                                            <label htmlFor="exampleFormControlTextarea10" className="form-label" style = {{paddingTop: "1rem"}}>2:</label>
                                                            <textarea defaultValue = {caseBuildingCtx.requirement.two} className="form-control" id="exampleFormControlTextarea10" rows={5} style = {{whiteSpace: "nowrap", resize: "none", width: "100%"}} ref = {reqReqtypePat2Ref}></textarea>
                                                        </div>
                                                        : null
                                                    }
                                                    {numberOfPatternReqTextAreas == 3 ? 
                                                        <div className="col" style = {{padding: "1px"}}>
                                                            <label htmlFor="exampleFormControlTextarea11" className="form-label" style = {{paddingTop: "1rem"}}>3:</label>
                                                            <textarea defaultValue = {caseBuildingCtx.requirement.three} className="form-control" id="exampleFormControlTextarea11" rows={5} style = {{whiteSpace: "nowrap", resize: "none", width: "100%"}} ref = {reqReqtypePat3Ref}></textarea>
                                                        </div>
                                                        : null
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        : null
                        }
                    </div>
                </div>
                <br></br>
                <div className = "card" style = {{width: "100%"}}>
                    <div className = "card-header">Add input variabe(s)</div>
                    <div className = "card-body">
                        <div className="container text-center">
                            <div className="row">
                                <div className="col-2">
                                    <button type="button" className="btn btn-info" onClick = {clearInputVariables}>Clear</button>
                                </div>
                                <div className="col-4" style = {{height: "100%"}}>
                                    {
                                        (inputVars.length > 0) ?
                                            <ul className="list-group" style = {{maxHeight: "10vh", overflow: "auto"}}>
                                                {
                                                    inputVars.map(inputVar => (
                                                        <li key = {inputVar} className="list-group-item">{inputVar}</li>
                                                    ))
                                                }
                                            </ul>
                                        :
                                        "No input variables created yet"
                                    }
                                </div>
                                <div className="col-4">
                                    <input ref = {inputVariableRef} type="text" className="form-control" id="exampleFormControlInput1" autoComplete="off" placeholder="Enter a variable" />
                                </div>
                                <div className="col-2">
                                    <button type="button" className="btn btn-info" onClick = {addInputVariable}>Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div className="card-footer text-body-secondary" style = {{background: colorThemeCtx.caseBuildFooterBackground}}>
                <div style = {{color: colorThemeCtx.darkModeTextColor}}>Go through all four steps. When you're done, build the case and then verify!</div>
            </div>
        </div>
    );
}

export default Requirement;



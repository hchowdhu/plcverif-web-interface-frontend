import classes from './UserInterface.module.css'
import flexClasses from '../FlexFlowControl.module.css'
import Menu from './menu/Menu';
import Workspace from './workspace/Workspace';
import { useContext } from 'react';
import ColorThemeContext from '../../store/color-theme-context';

function UserInterface() {

    const colorThemeCtx = useContext(ColorThemeContext);

    return (
        <div className = {["card mb-3", classes.uiPositionSize, flexClasses.uiFlexControl].join(" ")}>
            <div className = "row g-0" style = {{height: "100%"}}>
                <div className="col-md-3" style = {{background: colorThemeCtx.menuBackground}}>
                    <Menu />
                </div>
                <div className="col-md-9" style = {{background: colorThemeCtx.startBackground}}>
                    <Workspace />
                </div>
            </div>
        </div>
    );
}

export default UserInterface;
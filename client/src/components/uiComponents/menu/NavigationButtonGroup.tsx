import classes from './Menu.module.css'

function NavigationButtonGroup() {
    
    return (
        <div className = {["btn-group", classes.menuElement].join(" ")} role="group">
            <button type="button" className="btn btn-success" style = {{border: "1px solid black"}}>Code</button>
            <button type="button" className="btn btn-info" style = {{border: "1px solid black"}}>Case</button>
            <button type="button" className="btn btn-info" style = {{border: "1px solid black"}}>Report</button>
        </div>
    );
}

export default NavigationButtonGroup;
import { useContext } from "react";
import ColorThemeContext from "../../store/color-theme-context";

function Header() {

    const colorThemeCtx = useContext(ColorThemeContext);

    return (
        <div>
            <nav className="navbar" style = {{background: colorThemeCtx.headerBackground}}>
                <ul className="nav justify-content-end">
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link active" aria-current="page" href = "/">
                            <img src = "plcverif_icon.png" width = "40rem"/>
                        </a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href="https://gitlab.com/plcverif-oss" target = "_blank" style = {{color: "turquoise"}}>GitLab</a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href="https://plcverif-oss.gitlab.io/plcverif-docs/" target = "_blank" style = {{color: "turquoise"}}>Docs</a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href = "https://www.home.cern/" target = "_blank" style = {{color: "turquoise"}}>CERN</a>
                    </li>
                </ul>
            </nav>
            <p className="nav-link active" aria-current="page" 
                style = {{position: "absolute", top: "1.5rem", right: "1rem", textAlign: "right", color: "white"}}>
                CERN PLCVerif | Web Interface
            </p>
        </div>
    )
}

export default Header;

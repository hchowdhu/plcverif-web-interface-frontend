import { useContext } from "react"
import WorkspaceStateContext from "../../../../store/workspace-state-context"

function CaseInputWarnings() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);

    return (
        <ul className="list-group">
            {
                workspaceStateCtx.caseInputWarnings.map((warning) => (
                    <li className="list-group-item list-group-item-warning" style = {{marginTop: "2%"}} key = {warning}>
                        {
                            (warning == "output") ?
                                <div key = {warning}>You have stated the <code>-output</code> directory. Please remove it.</div>
                            : (warning == "sourcefiles_does_not_exist") ?
                                <div key = {warning}>No <code>-sourcefiles</code> attribute has been stated. Please include a source file.</div>
                            : (warning == "equals") ?
                                <div key = {warning}>You have an <code>=</code> sign with no space on the left, or the right, or both.</div>
                            : (warning == "sourcefiles_path") ?
                                <div key = {warning}>In the <code>-sourcefiles</code> attributes, remove any written path and only put the filename.</div>
                            : 
                            null
                        }
                    </li>
                ))
            }
        </ul>
    )
}

export default CaseInputWarnings;
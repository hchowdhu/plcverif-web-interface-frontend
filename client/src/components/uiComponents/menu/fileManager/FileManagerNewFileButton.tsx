import { useContext } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import ColorThemeContext from "../../../../store/color-theme-context";

function FileManagerNewFileButton() {
    
    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function createNewFile() {
        workspaceStateCtx.setCurrentState([0, 0]);
    }

    return (
        <button type="button" className="btn" style = {{border: "2px solid black", background: colorThemeCtx.newFileCodeCaseBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {createNewFile}>
            New file <img src = "newfile_button.png" style = {{height: "1.3rem"}} />
        </button>
    );
}

export default FileManagerNewFileButton;
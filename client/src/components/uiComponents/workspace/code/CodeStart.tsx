import { useContext, useRef } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import ColorThemeContext from "../../../../store/color-theme-context";

function CodeStart() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const codeFileInputRef = useRef<HTMLInputElement | null>(null);
    
    function readFileThenUpdateWorkspace(file: File | undefined) {
        // Upload file to context
        const reader = new FileReader();
        if(file != undefined){
            reader.readAsText(file);
            reader.addEventListener('load', (e) => {
                // Change name:
                // The RegEx pattern below is for if the the string is, 
                // "source", followed by a number, followed by ".scl"
                const pattern = /^source\d+\.scl$/;
                var modifiedName = pattern.test(file.name) ? (file.name.split(".")[0] + "A" + ".scl") : file.name;
                // Now check if the name already exists
                while(true){
                    if(workspaceStateCtx.nameExistsAlready(modifiedName, workspaceStateCtx.codes)){
                        modifiedName = modifiedName.split(".")[0] + "A" + ".scl";
                    } else {
                        break;
                    }
                }

                const data = e.target == null ? "" : e.target.result;
                const fileData = ((data == null ? "" : data).toString());
                // Initially, the first element is "" which should be replaced
                if(workspaceStateCtx.codes[0].name == "" && workspaceStateCtx.codes[0].code == "" && workspaceStateCtx.codes.length == 1){ 
                    workspaceStateCtx.setCurrentCodes([{name: modifiedName, code: fileData}]);
                } else {
                    workspaceStateCtx.addCode(modifiedName, fileData);
                }
                workspaceStateCtx.setCurrentState([0, 1]);
            });
        }
    }

    function executeClickAction() {
        (codeFileInputRef.current == null) ? null : codeFileInputRef.current.click();
    }
    
    function addBlankThenUpdateWorkspace() {
        if(workspaceStateCtx.codes.length > 0){
            // Initially, the first element is "" which should be replaced
            if(workspaceStateCtx.codes[0].name == "" && workspaceStateCtx.codes[0].code == "" && workspaceStateCtx.codes.length == 1){ 
                workspaceStateCtx.setCurrentCodes([{name: "source1.scl", code: ""}]);
            } else {
                workspaceStateCtx.addCode("", "");
            }
        } else { // if all files are deleted, and a new blank one is created
            workspaceStateCtx.addCode("", "");
        }
        workspaceStateCtx.setCurrentState([0, 1]);
    }

    return (
        <div className="container text-center">
            <h1 style = {{marginTop: "20vh", color: colorThemeCtx.darkModeTextColor}}>1. Add Source File(s)</h1>
            <div className="row" style = {{width: "50%", marginLeft: "25%", position: "relative", top: "5rem"}}>
                <div className="col">
                    <button type="button" className="btn btn-lg" style = {{width: "100%", height: "100%", background: colorThemeCtx.caseCodeStartButtonsBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {addBlankThenUpdateWorkspace}>
                        <h2>Start Blank</h2>
                    </button>
                </div>

                <div className="col">
                    <br></br>
                    <h2 style = {{color: colorThemeCtx.darkModeTextColor}}>or</h2>
                </div>
                
                <div className="col">
                    <input accept=".scl" type="file" ref = {codeFileInputRef} style = {{display: "none"}} 
                        onChange = {
                            (e) => {
                                readFileThenUpdateWorkspace(
                                    e.target == null ? 
                                        undefined : e.target["files"] == null ? 
                                            undefined : e.target["files"][0]);
                            }
                        }
                    />
                    <button type="button" className="btn btn-lg" style = {{width: "100%", height: "100%", background: colorThemeCtx.caseCodeStartButtonsBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {executeClickAction}>
                        <h2>Upload Existing</h2>
                    </button>
                </div>
            </div>
        </div>
    );
}

export default CodeStart;

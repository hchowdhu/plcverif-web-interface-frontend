import { useContext } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";

function CaseBuiltPopUp() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);

    return (
        <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header" style = {{background: "lightgreen"}}>
                        <h1 className="modal-title fs-5" id="exampleModalLabel"><b>Case created!</b></h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        You can now get the report by clicking "Verify"!
                        <br></br><br></br>
                        Here is a preview of your case file:
                        <textarea style = {{width: "100%"}} rows = {10} readOnly value = {workspaceStateCtx.case}></textarea>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CaseBuiltPopUp;
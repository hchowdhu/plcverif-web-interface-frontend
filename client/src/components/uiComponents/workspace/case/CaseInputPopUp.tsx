
function CaseInputPopUp() {

    return (
        <div className="modal fade" id="exampleModal2" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header" style = {{background: "lightred"}}>
                        <h1 className="modal-title fs-5" id="exampleModalLabel"><b>Note!</b></h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        Don't include the tag:
                        <br></br>
                        <code>-output</code> 
                        <br></br><br></br>
                        For the tag, <code>-sourcefiles</code>, write the file name after it with no path. For example,<br></br>
                        <code>-sourcefiles.0 = source1.scl</code>
                        <br></br><br></br>
                        Make sure to have a space before and after each equal sign of an attribute. For example,<br></br>
                        <code>-id = case1</code><br></br>
                        and not,<br></br>
                        <code>-id=case1</code>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" id = "caseInputModalPopUp">Close</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CaseInputPopUp;
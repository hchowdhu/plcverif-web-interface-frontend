import flexClasses from "../../../../FlexFlowControl.module.css"
import { useContext, useRef } from "react";
import CaseBuildingContext from "../../../../../store/caseBuilding-context";
import ColorThemeContext from "../../../../../store/color-theme-context";

function Metadata() {

    const caseBuildingCtx = useContext(CaseBuildingContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const mdIDRef = useRef(null);
    const mdNameRef = useRef(null);
    const mdDescriptionRef = useRef(null);

    function saveMetaDataInfo() {
        const mdIDRefVAL = mdIDRef.current == null ? "" : mdIDRef.current["value"];
        const mdNameRefVAL = mdNameRef.current == null ? "" : mdNameRef.current["value"];
        const mdDescriptionRefVAL = mdDescriptionRef.current == null ? "" : mdDescriptionRef.current["value"];

        caseBuildingCtx.setMetaData({
            id: mdIDRefVAL,
            name: mdNameRefVAL,
            description: mdDescriptionRefVAL
        });
    }

    return (
        <div className="card text-center" style = {{height: "100%", width: "85%", left: "7.5%", background: colorThemeCtx.caseBuildContentBackground}} onChange={saveMetaDataInfo}>
            <div className="card-header" style = {{background: colorThemeCtx.caseBuildHeaderBackground, color: colorThemeCtx.darkModeTextColor}}>
                <h5 style = {{marginTop: "1%"}}>1. Metadata</h5>
            </div>
            <div className = {["card-body", flexClasses.caseBuildContentFlexControl].join(" ")}>
                <div className="input-group mb-3" >
                    <span className="input-group-text" id="inputGroup-sizing-default">ID</span>
                    <input defaultValue = {caseBuildingCtx.metadata.id} type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" ref = {mdIDRef}/>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="inputGroup-sizing-default">Name</span>
                    <input defaultValue = {caseBuildingCtx.metadata.name} type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" ref = {mdNameRef}/>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="inputGroup-sizing-default">Description</span>
                    <textarea defaultValue = {caseBuildingCtx.metadata.description} className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" rows = {5} style = {{resize: "none"}} ref = {mdDescriptionRef}></textarea>
                </div>
            </div>
            <div className="card-footer text-body-secondary" style = {{background: colorThemeCtx.caseBuildFooterBackground}}>
                <div style = {{color: colorThemeCtx.darkModeTextColor}}>Go through all four steps. When you're done, build the case and then verify!</div>
            </div>
        </div>
    );
}

export default Metadata;



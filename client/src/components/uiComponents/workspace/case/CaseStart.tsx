import { useContext, useRef } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import ColorThemeContext from "../../../../store/color-theme-context";
import CaseInputPopUp from "./CaseInputPopUp";

function CaseStart() {

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const caseFileInputRef = useRef<HTMLInputElement | null>(null);
    
    function createUploadedCase(file: File | undefined) {
        workspaceStateCtx.setCurrentCase("");
        // Upload file to context
        const reader = new FileReader();
        if(file != undefined){
            reader.readAsText(file);
            reader.addEventListener('load', (e) => {
                const data = e.target == null ? "" : e.target.result;
                const fileData = ((data == null ? "" : data).toString());
                workspaceStateCtx.setCurrentCase(fileData);
                workspaceStateCtx.setCurrentState([1, 1]);
                workspaceStateCtx.setLastCaseWorkspaceState(1);
            });
        }
    }

    function executeClickAction() {
        (caseFileInputRef.current == null) ? null : caseFileInputRef.current.click();
    }

    function createBlankCase() {
        workspaceStateCtx.setCurrentCase("");
        workspaceStateCtx.setCurrentState([1, 1]);
        workspaceStateCtx.setLastCaseWorkspaceState(1);
    }

    function createBuildCase() {
        workspaceStateCtx.setCurrentCase("");
        workspaceStateCtx.setCurrentState([1, 2]);
        workspaceStateCtx.setLastCaseWorkspaceState(2);
    }

    return (
        <>
            <div className="container text-center">
                <h1 style = {{marginTop: "30vh", color: colorThemeCtx.darkModeTextColor}}>2. Add Case File</h1>
                <div className="row" style = {{width: "70%", marginLeft: "15%", position: "relative", top: "5rem"}}>

                    <div className="col">
                        <button type="button" className="btn btn-lg" style = {{width: "100%", height: "100%", background: colorThemeCtx.caseCodeStartButtonsBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {createBuildCase}>
                            <h2>Guided Build</h2>
                        </button>
                    </div>

                    <div className="col">
                        <br></br>
                        <h2 style = {{color: colorThemeCtx.darkModeTextColor}}>or</h2>
                    </div>

                    <div className="col">
                        <button data-bs-toggle="modal" data-bs-target="#exampleModal2" type="button" className="btn btn-lg" style = {{width: "100%", height: "100%", background: colorThemeCtx.caseCodeStartButtonsBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {createBlankCase}>
                            <h2>Start Blank</h2>
                        </button>
                    </div>

                    <div className="col">
                        <br></br>
                        <h2 style = {{color: colorThemeCtx.darkModeTextColor}}>or</h2>
                    </div>
                    
                    <div className="col">
                        <input accept=".vc3" type="file" ref = {caseFileInputRef} style = {{display: "none"}} 
                            onChange = {
                                (e) => {
                                    createUploadedCase(
                                        e.target == null ? 
                                            undefined : e.target["files"] == null ? 
                                                undefined : e.target["files"][0]);

                                    // Remove modal pop-up by programmatically clicking it
                                    document.getElementById("caseInputModalPopUp")?.click();
                                }
                            }
                        />
                        <button data-bs-toggle="modal" data-bs-target="#exampleModal2" type="button" className="btn btn-lg" style = {{width: "100%", height: "100%", background: colorThemeCtx.caseCodeStartButtonsBackground, color: colorThemeCtx.darkModeTextColor}} onClick = {executeClickAction}>
                            <h2>Upload Existing</h2>
                        </button>
                    </div>
                </div>
            </div>
            
            <CaseInputPopUp />
        </>
    );
}

export default CaseStart;

import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import { WorkspaceStateContextProvider } from './store/workspace-state-context.tsx'
import { CaseBuildingContextProvider } from './store/caseBuilding-context.tsx'
import { ColorThemeContextProvider } from './store/color-theme-context.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
	<WorkspaceStateContextProvider>
		<CaseBuildingContextProvider>
			<ColorThemeContextProvider>
				<div style = {{height: "100vh"}}>
					<App />
				</div>
			</ColorThemeContextProvider>
		</CaseBuildingContextProvider>
	</WorkspaceStateContextProvider>
)

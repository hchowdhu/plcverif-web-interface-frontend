import { useContext, useEffect, useState } from "react";
import WorkspaceStateContext from "../../../../store/workspace-state-context";
import ColorThemeContext from "../../../../store/color-theme-context";
import classes from './Report.module.css'

function Report() {   

    const [loading, setLoading] = useState(false);
    const [serverDown, setServerDown] = useState(false);
    const [height, setHeight] = useState<any>();

    const workspaceStateCtx = useContext(WorkspaceStateContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    useEffect(function() {
        const getReport = async () => {
            setLoading(true);

            var reportBody = document.getElementById("reportBody");
            if(reportBody != null) {
                reportBody.innerHTML = "";
            }

            var modifiedCase = workspaceStateCtx.case;

            // First identify which files need to be sent based on the case file's content
            const pattern = /-sourcefiles\.\d+ = (\S+)/g;
            const matches = [...modifiedCase.matchAll(pattern)];
            var files = [];
            var indexes = [];
            for (const match of matches) {
                files.push(match[1]);
                // Identify which index is the filename
                for(var i = 0; i < workspaceStateCtx.codes.length; i++) {
                    if(workspaceStateCtx.codes[i].name == match[1]){
                        indexes.push(i);
                        break;
                    }
                }
            }
            // Remove the -sourcefiles.n tags from the case file as they will be added later by the backend
            for(var i = 0; i < files.length; i++) {
                var stringToReplace = "-sourcefiles." + i + " = " + files[i];
                modifiedCase = modifiedCase.replace(stringToReplace, "");
            }
            // Next, store the code for the files which are to be sent
            var codes = []
            for(var i = 0; i < indexes.length; i++) {
                var fileIndex = indexes[i];
                codes.push(workspaceStateCtx.codes[fileIndex].code);
            }
            // Clear DOT files and all output files
            workspaceStateCtx.setDotFiles([]);
            workspaceStateCtx.setAllOutputFiles([]);
            
            // http://localhost:8082/
            // https://backend-plcverifwebinterface.app.cern.ch/
            const url = "https://backend-plcverifwebinterface.app.cern.ch/";

            // Post request for clearing the directory
            try {
                await fetch(url, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: "CLEAR_DIRECTORY"
                });
            } catch (err) {
                console.log("ErrorClearingDirectory: " + err);
            }
            
            // Post request for sending file names
            for(var i = 0; i < files.length; i++) {
                try {
                    await fetch(url, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: "FILE_NAME" + files[i]
                    });
                } catch (err) {
                    console.log("ErrorSendingFileName: " + err);
                }
            }

            // Post request for sending file codes
            for(var i = 0; i < codes.length; i++) {
                try {
                    await fetch(url, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: codes[i]
                    });
                } catch (err) {
                    console.log("ErrorSendingFileCode: " + err);
                }
            }

            // Post request for sending case file
            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: modifiedCase
                });
    
                if(response.ok) {
                    setServerDown(false);
                    setLoading(false);
    
                    var responseData = await response.text();
                    
                    if(reportBody != null) {
                        responseData = responseData
                            .replace("href=\"javascript:toggleVisibility('expertDetails')\"", "href = \"#\" onclick=\"document.getElementById('expertDetails').className=(document.getElementById('expertDetails').className=='hidden')?'':'hidden';\"")
                            .replace("href=\"javascript:toggleVisibility('expertDetails')\"", "href = \"#\" onclick=\"document.getElementById('expertDetails').className=(document.getElementById('expertDetails').className=='hidden')?'':'hidden';\"")
    
                        reportBody.innerHTML = responseData;
                    }
                }
            } catch (err) {
                console.log("ErrorVC3: " + err);
    
                setServerDown(true);
                setLoading(false);
            }

            // Post request for recieving DOT file
            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: "DOT_FILES_REQUEST"
                });

                if(response.ok) {
                    setServerDown(false);
                    setLoading(false);
    
                    var responseData = await response.text();
                    var responseDataParsed = responseData.split("NEW_DATA_HERE");
                    // The last element could be a text message, such as "error", avoid this
                    if(responseDataParsed.length % 2 != 0){
                        responseDataParsed.pop();
                    }
                    workspaceStateCtx.setDotFiles(responseDataParsed);
                }
            } catch (err) {
                console.log("ErrorDOTFiles: " + err);
            }

            // Post request for recieving all output files
            try {
                const response = await fetch(url, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: "ALL_FILES_REQUEST"
                });

                if(response.ok) {
                    setServerDown(false);
                    setLoading(false);
    
                    var responseData = await response.text();
                    var responseDataParsed = responseData.split("NEW_DATA_HERE");
                    // The last element could be a text message, such as "error", avoid this
                    if(responseDataParsed.length % 2 != 0){
                        responseDataParsed.pop();
                    }
                    workspaceStateCtx.setAllOutputFiles(responseDataParsed);
                }
            } catch (err) {
                console.log("ErrorAllFiles: " + err);
            }
        }
        
        getReport();
    }, []);

    useEffect(function() {
        var reportWrapper = document.getElementById("reportWrapper");
        setHeight(reportWrapper?.offsetHeight?.toString() + "px");
    }, []);

    return (
        <div id = "reportWrapper" className={"card " + classes.dimensionPosition} style = {{background: colorThemeCtx.startBackground, height: "100%"}}>
            {loading ? 
                <div className="card-body text-center">
                    <button className="btn btn-primary" type="button" disabled style = {{overflow: "hidden", whiteSpace: "nowrap"}}>
                        <span className="spinner-border spinner-border-sm" aria-hidden="true" style = {{fontSize: "5vw"}}></span>
                        <span role="status" style = {{paddingLeft: "1vw"}}>Waiting for report...</span>
                    </button>
                </div>  
            : 
            <div id = "reportBody" style = {{background: "white", position: "relative", top: "0rem", overflow: "auto", padding: "10px", height: height}}>{serverDown ? "Backend server is offline" : ""}</div>
            }
        </div>
    );
}

export default Report;